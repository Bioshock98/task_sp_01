# Task Manager
## software
+ JRE
+ Java 8
+ Maven 3.0
## developer
Pavel Pyshinskiy
pavel.pyshinskiy@gmail.com
## build application
```
mvn clean
mvn install
```
## run application
```
Put .war archive to webapp directory of Tomcat
```