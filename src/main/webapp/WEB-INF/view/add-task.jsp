<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adding new Task</title>
</head>
<body>
    <%--@elvariable id="task" type="ru.pyshinskiy.tm.model.Task"--%>
    <form:form method="POST" action="/task/save" modelAttribute="task">
        <p>
            NAME:
        </p>
        <p>
            <form:input path="name" />
        </p>
        <p>
            DESCRIPTION:
        </p>
        <p>
            <form:input path="description" />
        </p>
        <p>
            START DATE:
        </p>
        <p>
            <form:input path="startDate"/>
        </p>
        <p>
            FINISH DATE:
        </p>
        <p>
            <form:input path="finishDate"/>
        </p>
        <p>
            <form:select path="project">
                <%--@elvariable id="projects" type="java.util.List"--%>
                <form:options items="${projects}" itemValue="id" itemLabel="name"/>
            </form:select>
        </p>
        <p>
            <button type="submit">SUBMIT</button>
        </p>
    </form:form>
</body>
</html>
