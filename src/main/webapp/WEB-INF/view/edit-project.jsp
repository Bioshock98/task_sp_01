<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%--@elvariable id="project" type="ru.pyshinskiy.tm.model.Project"--%>
    <form:form method="POST" action="/project/save" modelAttribute="project">
        <form:hidden path="id"/>
        <p>
            EDIT NAME:
        </p>
        <p>
            <form:input path="name"/>
        </p>
        <p>
            EDIT DESCRIPTION:
        </p>
            <form:input path="description"/>
        <p>
            EDIT START DATE
        </p>
        <p>
            <form:input path="startDate" />
        </p>
        <p>
            EDIT FINISH DATE
        </p>
        <p>
            <form:input path="finishDate" />
        </p>
        <p>
            EDIT STATUS:
        </p>
        <p>
            <form:input path="status" />
        </p>
        <p>
            <button type="submit">SUBMIT</button>
        </p>
    </form:form>
</body>
</html>