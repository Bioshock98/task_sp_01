<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%--@elvariable id="task" type="ru.pyshinskiy.tm.model.Task"--%>
    <form:form method="POST" action="/task/save" modelAttribute="task">
        <form:hidden path="id"/>
        <p>
            EDIT NAME:
        </p>
        <p>
            <form:input path="name"/>
        </p>
        <p>
            EDIT DESCRIPTION:
        </p>
            <form:input path="description"/>
        <p>
            EDIT START DATE
        </p>
        <p>
            <form:input path="startDate" />
        </p>
        <p>
            EDIT FINISH DATE
        </p>
        <p>
            <form:input path="finishDate" />
        </p>
        <p>
            EDIT STATUS:
        </p>
        <p>
            <form:input path="status" />
        </p>
        <p>
            EDIT ATTACHING PROJECT:
        </p>
        <p>
            <form:select path="project">
                <%--@elvariable id="projects" type="java.util.List"--%>
                <form:options items="${projects}" itemValue="id" itemLabel="name"/>
            </form:select>
        </p>
        <p>
            <button type="submit">SUBMIT</button>
        </p>
    </form:form>
</body>
</html>
