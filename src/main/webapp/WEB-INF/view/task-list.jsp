<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task List</title>
</head>
<body>
    <h3>TASKS</h3>
    <hr />
    <br />
    <a href="/project/list">Project list</a>
    <a href="/task/list">Task list</a>
    <br />
    <hr />
    <br />
    <br />
    <div>
        <a href="/task/addForm">Add Task</a>
        <table>
            <tr>
                <th>Task name</th>
                <th>Project</th>
                <th>Description</th>
                <th>Status</th>
                <th>startDate</th>
                <th>finishDate</th>
            </tr>
            <%--@elvariable id="tasks" type="java.util.List"--%>
            <c:forEach var="task" items="${tasks}">
                <c:url var="editLink" value="/task/edit">
                    <c:param name="taskId" value="${task.getId()}" />
                </c:url>
                <c:url var="deleteLink" value="/task/delete">
                    <c:param name="taskId" value="${task.getId()}" />
                </c:url>
                <tr>
                    <td>${task.getName()}</td>
                    <td>${task.getProject().getName()}</td>
                    <td>${task.getDescription()}</td>
                    <td>${task.getStatus()}</td>
                    <td>${task.getStartDate()}</td>
                    <td>${task.getFinishDate()}</td>
                    <td>
                        <a href="${editLink}">Edit</a>
                        <a href="${deleteLink}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
