<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>Adding new Project</h3>

<%--@elvariable id="project" type="ru.pyshinskiy.tm.model.Project"--%>
<form:form method="POST" action="/project/save" modelAttribute="project">
    <p>
        NAME:
    </p>
    <p>
        <form:input path="name" />
    </p>
    <p>
        DESCRIPTION:
    </p>
    <p>
        <form:input path="description" />
    </p>
    <p>
        START DATE:
    </p>
    <p>
        <form:input path="startDate"/>
    </p>
    <p>
        FINISH DATE:
    </p>
    <p>
        <form:input path="finishDate"/>
    </p>
    <p>
        <button type="submit">SUBMIT</button>
    </p>
</form:form>
</body>
</html>
