<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Project list</title>
</head>
<body>
    <h3>PROJECTS</h3>
    <hr />
    <br />
    <a href="/project/list">Project list</a>
    <a href="/task/list">Task list</a>
    <br />
    <hr />
    <br />
    <br />
    <div>
        <a href="/project/addForm">Add Project</a>
        <table>
            <tr>
                <th>Project name</th>
                <th>Description</th>
                <th>Status</th>
                <th>startDate</th>
                <th>finishDate</th>
                <th>tasks</th>
            </tr>
            <%--@elvariable id="projects" type="java.util.List"--%>
            <c:forEach var="project" items="${projects}">
                <c:url var="editLink" value="/project/edit">
                    <c:param name="projectId" value="${project.getId()}" />
                </c:url>
                <c:url var="deleteLink" value="/project/delete">
                    <c:param name="projectId" value="${project.getId()}" />
                </c:url>
                <tr>
                    <td>${project.getName()}</td>
                    <td>${project.getDescription()}</td>
                    <td>${project.getStatus()}</td>
                    <td>${project.getStartDate()}</td>
                    <td>${project.getFinishDate()}</td>
                    <td>
                        <table>
                            <c:forEach var="task" items="${project.getTasks()}">
                                <tr>
                                    <td>${task.getName()}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </td>
                    <td>
                        <a href="${editLink}">Edit</a>
                        <a href="${deleteLink}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
