package ru.pyshinskiy.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.project.IProjectService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @GetMapping("/list")
    public String listProjects(Model model) {
        @NotNull  List<Project> projects = null;
        try {
            projects = projectService.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/addForm")
    public String showAddingForm(Model model) {
        @NotNull final Project project = new Project();
        model.addAttribute("project", project);
        return "add-project";
    }

    @PostMapping("/save")
    public String saveProject(@ModelAttribute("project") @NotNull final Project project) {
        try {
            projectService.save(project);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/project/list";
    }

    @GetMapping("/edit")
    public String editProject(@RequestParam("projectId") @NotNull final String projectId,
                                    Model model) {
        @Nullable Project project = null;
        try {
            project = projectService.findOne(projectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("project", project);
        return "edit-project";
    }

    @GetMapping("/delete")
    public String deleteProject(@RequestParam("projectId") @NotNull final String projectId) {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        try {
            projectService.remove(project);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/project/list";
    }
}
