package ru.pyshinskiy.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.task.ITaskService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @GetMapping("/list")
    public String listTask(Model model) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        try {
            tasks.addAll(taskService.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/addForm")
    public String showAddingForm(Model model) {
        @NotNull final List<Project> projects = new ArrayList<>();
        try {
            projects.addAll(projectService.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
        @NotNull final Task task = new Task();
        model.addAttribute("task", task);
        model.addAttribute("projects", projects);
        return "add-task";
    }

    @PostMapping("/save")
    public String saveTask(@ModelAttribute("task") @NotNull final Task task) {
        try {
            taskService.save(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/project/list";
    }

    @GetMapping("/edit")
    public String editTask(@RequestParam("taskId") @NotNull final String taskId, Model model) {
        @NotNull final List<Project> projects = new ArrayList<>();
        @Nullable Task task = null;
        try {
            task = taskService.findOne(taskId);
            projects.addAll(projectService.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("task", task);
        model.addAttribute("projects", projects);
        return "edit-task";
    }

    @GetMapping("/delete")
    public String deleteTask(@RequestParam("taskId") @NotNull final String taskId) {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        try {
            taskService.remove(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/project/list";
    }
}
