package ru.pyshinskiy.tm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.pyshinskiy.tm.formatter.ProjectFormatter;

@EnableWebMvc
@ComponentScan(basePackages = "ru.pyshinskiy.tm")
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    ProjectFormatter projectFormatter;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(projectFormatter);
    }

    @Bean
    public InternalResourceViewResolver resolver() {
        final InternalResourceViewResolver resolver =
                new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/view/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
}
