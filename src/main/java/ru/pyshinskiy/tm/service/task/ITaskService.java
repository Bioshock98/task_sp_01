package ru.pyshinskiy.tm.service.task;

import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.service.IService;

public interface ITaskService extends IService<Task> {
}
