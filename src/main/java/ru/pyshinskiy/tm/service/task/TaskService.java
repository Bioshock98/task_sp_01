package ru.pyshinskiy.tm.service.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        return taskRepository.findTaskById(id);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return (List<Task>) taskRepository.findAll();
    }

    @Transactional
    @Override
    public void save(@Nullable final Task project) throws Exception {
        if(project == null) throw new Exception("invalid task");
        taskRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final Task project) throws Exception {
        if(project == null) throw new Exception("project is null");
        taskRepository.delete(project);
    }

    @Transactional
    @Override
    public void removeAll() {
        taskRepository.deleteAll();
    }
}
