package ru.pyshinskiy.tm.service.project;

import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.IService;

public interface IProjectService extends IService<Project> {
}
