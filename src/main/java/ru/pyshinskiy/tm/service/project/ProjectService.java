package ru.pyshinskiy.tm.service.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        return projectRepository.findProjectById(id);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return (List<Project>) projectRepository.findAll();
    }

    @Transactional
    @Override
    public void save(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        projectRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("project is null");
        projectRepository.delete(project);
    }

    @Transactional
    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }
}
