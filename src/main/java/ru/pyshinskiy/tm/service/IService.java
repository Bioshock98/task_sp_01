package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @Nullable
    T findOne(@Nullable final String id) throws Exception;

    @NotNull
    List<T> findAll() throws Exception;

    void save(@Nullable final T t) throws Exception;

    void remove(@Nullable final T t) throws Exception;
}
