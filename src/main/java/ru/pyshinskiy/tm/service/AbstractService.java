package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractService<T> implements IService<T> {

    @Nullable
    abstract public T findOne(@Nullable final String id) throws Exception;

    @NotNull
    abstract public List<T> findAll() throws Exception;

    abstract public void save(@Nullable final T t) throws Exception;

    abstract public void remove(@Nullable final T t) throws Exception;

    abstract public void removeAll() throws Exception;
}
